//
//  ConsoleListCell.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/10/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "ConsoleListCell.h"

@implementation ConsoleListCell



- (IBAction)valueChanged:(UISwitch *)sender {
    if(!sender.on){
        NSMutableArray *consoles = [self getFile];
        
        for (NSString *console in consoles) {
            NSLog(@"%@", console);
        }
        
        [consoles removeObject:self.consoleLBL.text];
        
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [documentPaths objectAtIndex:0];
        NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"ConsoleForSearch.plist"];
        
        [consoles writeToFile:documentPlistPath atomically:YES];
        consoles = [self getFile];
        
        for (NSString *console in consoles) {
            NSLog(@"%@", console);
        }
    }
    else {
        
        NSMutableArray *consoles = [self getFile];
        
        [consoles addObject:self.consoleLBL.text];
        
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [documentPaths objectAtIndex:0];
        NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"ConsoleForSearch.plist"];
        
        [consoles writeToFile:documentPlistPath atomically:YES];
    }
}

-(NSMutableArray *) getFile {
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"ConsoleForSearch.plist"];
    NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
    return documentDict;
}

@end
