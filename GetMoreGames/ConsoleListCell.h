//
//  ConsoleListCell.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/10/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsoleListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *consoleLBL;
@property (weak, nonatomic) IBOutlet UISwitch *cellSwitch;

- (IBAction)valueChanged:(UISwitch *)sender;

@end
