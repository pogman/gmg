//
//  DAO.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/11/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "DAO.h"

//static NSString *baseUrl = @"https://fast-eyrie-25854.herokuapp.com/api";
static NSString *baseUrl = @"http://localhost:3000/api";

@implementation DAO

+ (void)listGames:(void (^)(BOOL success, NSArray *json)) completionHandler {
    NSString *url = [NSString stringWithFormat:@"%@/product/list", baseUrl];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *json = [[NSMutableArray alloc] init];
        json = (NSMutableArray *) responseObject;
        completionHandler(YES, json);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        completionHandler(NO, nil);
        NSLog(@"Error: %@", error);
    }];
}

+ (void) listGameByConsole: (NSString *)name andPlatforms: (NSMutableArray *)platforms andCompletionHandler: (void (^)(BOOL success, NSArray *json)) completionHandler {
    NSString *url = [NSString stringWithFormat:@"%@/product/search", baseUrl];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSDictionary *params = @{@"name": name, @"platforms": [platforms componentsJoinedByString:@","]};
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *json = [[NSMutableArray alloc] init];
        json = (NSMutableArray *) responseObject;
        completionHandler(YES, json);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        completionHandler(NO, nil);
        NSLog(@"Error: %@", error);
    }];
}

+ (void) getUserById: (NSString *) userID andCompletionHandler: (void (^)(BOOL success, NSDictionary *json)) completionHandler {
    NSString *url = [NSString stringWithFormat: @"%@/user/%@", baseUrl, userID];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *json = [[NSDictionary alloc] init];
        json = (NSDictionary *) responseObject;
        completionHandler(YES, json);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        completionHandler(NO, nil);
        NSLog(@"Error: %@", error);
    }];
}

+ (void) addUser: (NSString *)name email: (NSString *)email password: (NSString *)password
         location: (NSString *)location phone: (NSString *)phone image: (NSString *)image andCompletionHandler: (void (^)(BOOL success, NSDictionary *json)) completionHandler {
    NSString *url = [NSString stringWithFormat:@"%@/user/create", baseUrl];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *params = @{@"name": name, @"email": email, @"password": password, @"location": location, @"phone_number": phone, @"image": image};
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *json = [[NSDictionary alloc] init];
        json = (NSDictionary *) responseObject;
        completionHandler(YES, json);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error: %@", error);
        completionHandler(NO, nil);
    }];
}

+ (void) addGame: (NSString *)name price: (NSString *)price platform: (NSString *)platform image: (NSString *)image userId: (NSString *)userId andCompletionHandler:(void (^)(BOOL success, NSDictionary *json)) completionHandler {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *url = [NSString stringWithFormat:@"%@/product/%@/create", baseUrl, userId];
    
    NSDictionary *params = @{@"name": name, @"price": price, @"platform": platform, @"image": image};
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *json = [[NSDictionary alloc] init];
        json = (NSDictionary *) responseObject;
        completionHandler(YES, json);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error: %@", error);
        completionHandler(NO, nil);
    }];
}

+ (void)login: (NSString *)email  password: (NSString *)password andCompletionHandler: (void (^)(BOOL success, NSDictionary *json)) completionHandler {
    NSString *url = [NSString stringWithFormat:@"%@/user/login", baseUrl];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *params = @{@"email": email, @"password": password};
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *json = [[NSDictionary alloc] init];
        json = (NSDictionary *) responseObject;
        completionHandler(YES, json);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error: %@", error);
        completionHandler(NO, nil);
    }];
}

@end
