//
//  ProfileVC.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "ProfileVC.h"

@implementation ProfileVC

-(void)viewWillAppear:(BOOL)animated{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Carregando";
    
    CALayer *imageLayer = self.profilePictureIMG.layer;
    [imageLayer setCornerRadius:self.profilePictureIMG.frame.size.width/2];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [self.profilePictureIMG.layer setMasksToBounds:YES];
    
    [DAO getUserById: self.user.userID andCompletionHandler:^(BOOL success, NSDictionary *json) {
        self.nameLBL.text = [json objectForKey:@"name"];
        self.phoneLBL.text = [json objectForKey:@"phone_number"];
        self.addressLBL.text = [json objectForKey:@"location"];
        self.mailLBL.text = [json objectForKey:@"email"];
        NSData *data = [[NSData alloc]initWithBase64EncodedString:[json objectForKey:@"image"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        self.profilePictureIMG.image =[self imageWithImage:[UIImage imageWithData: data] scaledToSize:CGSizeMake(80.0, 90.0)];
        [hud hide:YES];
    }];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
