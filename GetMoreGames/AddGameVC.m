//
//  AddGameVC.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "AddGameVC.h"
#import "ConsoleVC.h"

@implementation AddGameVC

-(void)viewDidLoad{
    self.console = [[NSString alloc] init];
    
    self.valueTF.delegate = self;
    self.nameTF.delegate = self;
    
    self.nameTF.leftViewMode = UITextFieldViewModeAlways;
    self.valueTF.leftViewMode = UITextFieldViewModeAlways;
    
    self.nameTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disc-29.png"]];
    self.valueTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"price-29.png"]];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImage:)];
    singleTap.numberOfTapsRequired = 1;
    [self.gamePhotoIMG setUserInteractionEnabled:YES];
    [self.gamePhotoIMG addGestureRecognizer:singleTap];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)setString:(NSString *)console{
    self.console = console;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.valueTF.text.length  == 0 ) {
        self.valueTF.text = [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol];
    }
}

- (IBAction)consoleSelection:(UIButton *)sender {
    [self performSegueWithIdentifier:@"console" sender:nil];
}

- (IBAction)addGame:(UIButton *)sender {
    NSMutableArray *profile = [self getFile];
    NSString *image = [UIImagePNGRepresentation(self.gamePhotoIMG.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    if((self.valueTF.text.length == 0 || self.nameTF.text.length == 0 || self.consoleBTN.titleLabel.text.length == 17)){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Falha de cadastro."
                                                                       message:@"Preencha os campos para cadastrar um jogo."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];

    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Enviando";
        [DAO addGame:self.nameTF.text price:self.valueTF.text platform:self.consoleBTN.titleLabel.text image:image userId:[profile objectAtIndex:0] andCompletionHandler:^(BOOL success, NSDictionary *json) {
            if (!success) {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Falha de cadastro."
                                                                               message:@"Não foi possível cadastrar o jogo."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Cadastro realizado"
                                                                               message:@"Cadastro realizado com sucesso."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                
                [self.consoleBTN setTitle:@"Selecione o console" forState:UIControlStateNormal];
                [self.nameTF setText:@""];
                [self.valueTF setText:@""];
                self.gamePhotoIMG.image = [UIImage imageNamed:@"defaultImage-128"];
            }
            [hud hide:YES];
        }];

    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"gameDetail"]){
        ConsoleVC *consoleVC = [segue destinationViewController];
        consoleVC.delegate = self;
        consoleVC.console = self.console;
    }
    
}

-(IBAction) catchConsole:(UIStoryboardSegue *)segue{
    ConsoleVC *consoleVC = [segue sourceViewController];
    [self.consoleBTN setTitle: consoleVC.console forState:UIControlStateNormal];
}


- (IBAction)selectImage:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Escolher imagem" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Galeria" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.gamePhotoIMG.image = selectedImage;
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)showcamera {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setAllowsEditing:YES];
    
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

-(NSMutableArray *) getFile{
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"Profile.plist"];
    NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
    
    return documentDict;
}

@end
