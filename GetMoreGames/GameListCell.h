//
//  GameListCell.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/4/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *gameImage;
@property (weak, nonatomic) IBOutlet UILabel *tittleLBL;
@property (weak, nonatomic) IBOutlet UILabel *consoleLBL;
@property (weak, nonatomic) IBOutlet UILabel *valueLBL;
@property (weak, nonatomic) IBOutlet UILabel *addressLBL;

@end
