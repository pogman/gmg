//
//  ConfigVC.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfigVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *consoleTV;

@property (strong, nonatomic) NSMutableArray *consoleArray;

@end
