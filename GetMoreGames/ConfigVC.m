//
//  ConfigVC.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "ConfigVC.h"
#import "ConsoleListCell.h"

@implementation ConfigVC

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSMutableArray *consoles = [NSMutableArray arrayWithContentsOfFile:[bundle pathForResource:@"ConsoleList" ofType:@"plist"]];
    self.consoleArray = [[NSMutableArray alloc] initWithArray:consoles];
    
    self.consoleTV.backgroundColor = [UIColor clearColor];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.consoleArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray *consolesForSearch = [self getFile];
    
    ConsoleListCell *cell = (ConsoleListCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[ConsoleListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
 
    
    
    NSString *console = [self.consoleArray objectAtIndex:indexPath.row];
    cell.consoleLBL.text = console;
    
    if(![consolesForSearch containsObject:console]){
        [cell.cellSwitch setOn:NO];
        
    }
        
    return cell;
}

-(NSMutableArray *) getFile{
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"ConsoleForSearch.plist"];
    NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
    return documentDict;
}

-(NSMutableArray *) writeToFile{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"ConsoleForSearch.plist"];
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    NSString *bundlePlistPath = [bundlePath stringByAppendingPathComponent:@"ConsoleList.plist"];
    
    //if file exists in the documents directory, get it
    if([fileManager fileExistsAtPath:documentPlistPath]){
        NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
        return documentDict;
    }
    //if file does not exist, create it from existing plist
    else {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:bundlePlistPath toPath:documentPlistPath error:&error];
        if (success) {
            NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
            return documentDict;
        }
        return nil;
    }
}

@end
