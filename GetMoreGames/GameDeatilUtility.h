//
//  GameDeatilUtility.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/10/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface GameDeatilUtility : NSObject

@property (strong, nonatomic) UIImageView *gameImage;

@property (strong, nonatomic) NSString *value;

@property (strong, nonatomic) NSString *phone;

@property (strong, nonatomic) NSString *address;

@property (strong, nonatomic) NSString *ownerName;

@property (strong, nonatomic) NSString *gameName;

@property (strong, nonatomic) NSString *ownerID;
@end
