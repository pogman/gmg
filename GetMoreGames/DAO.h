//
//  DAO.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/11/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface DAO : NSObject

+ (void)listGames:(void (^)(BOOL success, NSArray *json)) completionHandler;

+ (void) getUserById: (NSString *) userID andCompletionHandler: (void (^)(BOOL success, NSDictionary *json)) completionHandler;

+ (void) addUser: (NSString *)name email: (NSString *)email password: (NSString *)password
        location: (NSString *)location phone: (NSString *)phone image: (NSString *)image andCompletionHandler: (void (^)(BOOL success, NSDictionary *json)) completionHandler;

+ (void) addGame: (NSString *)name price: (NSString *)price platform: (NSString *)platform image: (NSString *)image userId: (NSString *)userId andCompletionHandler:(void (^)(BOOL success, NSDictionary *json)) completionHandler;

+ (void) listGameByConsole: (NSString *)name andPlatforms: (NSMutableArray *)platforms andCompletionHandler: (void (^)(BOOL success, NSArray *json)) completionHandler;

+ (void)login: (NSString *)email  password: (NSString *)password andCompletionHandler: (void (^)(BOOL success, NSDictionary *json)) completionHandler;

@end
