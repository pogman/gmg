//
//  ConsoleVC.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "ConsoleVC.h"
#import "ConsoleListCell.h"
#import "AddGameVC.h"


@implementation ConsoleVC
-(void)viewWillDisappear:(BOOL)animated{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.consoleTV.backgroundColor = [UIColor clearColor];
    NSBundle *bundle = [NSBundle mainBundle];
    NSMutableArray *consoles = [NSMutableArray arrayWithContentsOfFile:[bundle pathForResource:@"ConsoleList" ofType:@"plist"]];
    self.consoleArray = [[NSMutableArray alloc] initWithArray:consoles];
}

-(void)viewDidLoad{
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.consoleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ConsoleListCell *cell = (ConsoleListCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[ConsoleListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.backgroundColor = [UIColor clearColor];
    NSString *console = [self.consoleArray objectAtIndex:indexPath.row];
    cell.consoleLBL.text = console;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ConsoleListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    self.console = cell.consoleLBL.text;
    
    for (UIViewController* viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[AddGameVC class]] ) {
            [self performSegueWithIdentifier:@"comeBack" sender:self];
        }
    }
}

@end
