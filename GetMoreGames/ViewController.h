//
//  ViewController.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/4/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "DAO.h"
#import "UserData.h"

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *logoIMG;


@property (weak, nonatomic) IBOutlet UITextField *emailTF;

@property (weak, nonatomic) IBOutlet UITextField *passwordTF;

@property (weak, nonatomic) IBOutlet UIButton *loginBTN;

@property UserData *user;

@property (strong, nonatomic) NSMutableArray *profile;

- (IBAction)login:(UIButton *)sender;

- (IBAction)signUp:(UIButton *)sender;

@end

