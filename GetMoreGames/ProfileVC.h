//
//  ProfileVC.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserData.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "DAO.h"

@interface ProfileVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *profilePictureIMG;

@property (weak, nonatomic) IBOutlet UILabel *nameLBL;

@property (weak, nonatomic) IBOutlet UILabel *mailLBL;

@property (weak, nonatomic) IBOutlet UILabel *phoneLBL;

@property (weak, nonatomic) IBOutlet UILabel *addressLBL;

@property (weak, nonatomic) IBOutlet UILabel *likeLBL;

@property (weak, nonatomic) IBOutlet UILabel *dislikeLBL;

@property (strong, nonatomic) UserData *user;

@end
