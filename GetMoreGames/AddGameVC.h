//
//  AddGameVC.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "DAO.h"

@interface AddGameVC : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *gamePhotoIMG;

@property (weak, nonatomic) IBOutlet UITextField *nameTF;

@property (weak, nonatomic) IBOutlet UITextField *valueTF;

@property (weak, nonatomic) IBOutlet UIButton *consoleBTN;

@property (strong, nonatomic)NSString *console;

@property BOOL hasLoadedCamera;

- (IBAction)consoleSelection:(UIButton *)sender;

- (IBAction)addGame:(UIButton *)sender;

- (IBAction) catchConsole:(UIStoryboardSegue *) segue;

- (IBAction)selectImage:(UIButton *)sender;

@end
