//
//  SignUpVC.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/6/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "SignUpVC.h"

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameTF.leftViewMode = UITextFieldViewModeAlways;
    self.emailTF.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTF.leftViewMode = UITextFieldViewModeAlways;
    self.numberTF.leftViewMode = UITextFieldViewModeAlways;
    self.addressTF.leftViewMode = UITextFieldViewModeAlways;
    
    self.nameTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profile3-29.png"]];
    self.emailTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email-29.png"]];
    self.passwordTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock-29.png"]];
    self.numberTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone2-29.png"]];
    self.addressTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location-29.png"]];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImage:)];
    singleTap.numberOfTapsRequired = 1;
    [self.profileIMG setUserInteractionEnabled:YES];
    [self.profileIMG addGestureRecognizer:singleTap];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)selectImage:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Escolher imagem" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Galeria" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.profileIMG.image = selectedImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



- (IBAction)signUp:(UIButton *)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *image = [UIImagePNGRepresentation(self.profileIMG.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Carregando";

    
    [DAO addUser:self.nameTF.text email:self.emailTF.text password:self.passwordTF.text
        location:self.addressTF.text phone:self.numberTF.text image:image andCompletionHandler:^(BOOL success, NSDictionary *json) {
            if (json == nil) {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Falha ao cadastrar"
                                                                               message:@"Não foi possível completar seu cadastro."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Falha de cadastro"
                                                                               message:@"Não foi possível executar o cadastro.."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];


                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            [hud hide:YES];
            
    }];
}
@end
