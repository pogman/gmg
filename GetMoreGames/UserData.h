//
//  UserData.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/11/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UserData : NSObject

@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) NSString *phone;

@property (strong, nonatomic) NSString *address;

@property (strong, nonatomic) NSString *email;

@property (strong, nonatomic) NSString *userID;

@property (strong, nonatomic) UIImage *userImage;

@end
