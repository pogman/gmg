//
//  ViewController.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/4/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CALayer *imageLayer = self.logoIMG.layer;
    [imageLayer setCornerRadius:self.logoIMG.frame.size.width/2];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [self.logoIMG.layer setMasksToBounds:YES];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    self.emailTF.delegate = self;
    self.passwordTF.delegate = self;
    
    self.emailTF.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTF.leftViewMode = UITextFieldViewModeAlways;
    
    self.emailTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email-29.png"]];
    self.passwordTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock-29.png"]];
    
    self.profile = [[NSMutableArray alloc] init];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)login:(UIButton *)sender {
    if(![self.emailTF.text isEqualToString:@""] && ![self.passwordTF.text isEqualToString:@""]){
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Carregando";
        [DAO login:self.emailTF.text password:self.passwordTF.text andCompletionHandler:^(BOOL success, NSDictionary *json) {
            
            if (!(json == nil)) {
                NSBundle *bundle = [NSBundle mainBundle];
                
                self.user = [[UserData alloc] init];
                self.user.name = [json objectForKey:@"name"];
                self.user.phone = [json objectForKey:@"phone_number"];
                self.user.address = [json objectForKey:@"localtion"];
                self.user.email = [json objectForKey:@"email"];
                self.user.userID = [json objectForKey:@"id"];
                [self.profile addObject:self.user.userID];
                
                [self.profile writeToFile:[bundle pathForResource:@"Profile" ofType:@"plist"] atomically:YES];
                [self writeToFile];
                [self performSegueWithIdentifier:@"selectGame" sender:sender];
            }
            else {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Falha de login"
                                                                               message:@"Email ou senha incorretos."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            [hud hide:YES];
        }];
        
    }
}

-(NSMutableArray *) writeToFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"Profile.plist"];
    NSMutableArray *userArray = [[NSMutableArray alloc] initWithObjects:self.user.userID, nil];
    
    if([fileManager fileExistsAtPath:documentPlistPath]){
        NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
        [documentDict removeAllObjects];
        [documentDict addObject:self.user.userID];
        [documentDict writeToFile:documentPlistPath atomically:YES];
        documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
        return documentDict;
    }
    else {
        BOOL success = [userArray writeToFile:documentPlistPath atomically:YES];
        if (success) {
            NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
            return documentDict;
        }
        return nil;
    }
}

- (IBAction)signUp:(UIButton *)sender {
    [self performSegueWithIdentifier:@"signUp" sender:sender];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
