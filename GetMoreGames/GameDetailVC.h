//
//  GameDetailVC.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "GameDeatilUtility.h"
#import "DAO.h"
#import "UserData.h"

@interface GameDetailVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *gamePhotoIMG;

@property (strong, nonatomic) UIImage *gameImage;

@property (strong, nonatomic) IBOutlet UILabel *gameNameLBL;

@property (weak, nonatomic) IBOutlet UILabel *valueLBL;

@property (weak, nonatomic) IBOutlet UIButton *profileBTN;

@property (weak, nonatomic) IBOutlet UILabel *phoneLBL;

@property (weak, nonatomic) IBOutlet UILabel *addressLBL;

@property (strong, nonatomic) GameDeatilUtility *game;

@property (strong, nonatomic) UserData *user;

- (IBAction)profile:(UIButton *)sender;

@end
