//
//  GameDetailVC.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "GameDetailVC.h"
#import "ProfileVC.h"

@implementation GameDetailVC

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.profileBTN.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Carregando";
    [DAO getUserById:self.game.ownerID andCompletionHandler:^(BOOL success, NSDictionary *json) {
        self.user.name = [json objectForKey:@"name"];
        self.user.phone = [json objectForKey:@"phone_number"];
        self.user.address = [json objectForKey:@"location"];
        self.user.email = [json objectForKey:@"email"];
        self.user.userID = [json objectForKey:@"id"];

        self.gameNameLBL.text = self.game.gameName;
        self.valueLBL.text = self.game.value;
        self.gamePhotoIMG.image = self.gameImage;
        self.valueLBL.text = self.game.value;
        
        NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:self.user.name attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid)}];
        [self.profileBTN setAttributedTitle:titleString forState:UIControlStateNormal];
        
        self.phoneLBL.text = self.user.phone;
        self.addressLBL.text = self.user.address;
        
        self.gamePhotoIMG.image = self.gameImage;
        
        [hud hide:YES];
    }]; 
    
}

-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)profile:(UIButton *)sender {
    [self performSegueWithIdentifier:@"profile" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ProfileVC *profile = [segue destinationViewController];
    profile.user = self.user;
}
@end
