//
//  SelectGameVC.m
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/4/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import "SelectGameVC.h"
#import "GameListCell.h"
#import "GameDeatilUtility.h"
#import "GameDetailVC.h"
#import "ProfileVC.h"

@implementation SelectGameVC

-(void)viewDidLoad {
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.consoles = [self writeToFile];
    
    self.filteredGames = [[NSMutableArray alloc] init];
    
    self.game = [[GameDeatilUtility alloc] init];
    self.user = [[UserData alloc] init];
    
    self.profile = [[NSMutableArray alloc] init];
}

-(void) startHUD {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.labelText = @"Carregando";
}

-(NSMutableArray *) writeToFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"ConsoleForSearch.plist"];
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    NSString *bundlePlistPath = [bundlePath stringByAppendingPathComponent:@"ConsoleList.plist"];
    
    if([fileManager fileExistsAtPath:documentPlistPath]){
        NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];

        return documentDict;
    }
    else {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:bundlePlistPath toPath:documentPlistPath error:&error];
        if (success) {
            NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
            return documentDict;
        }
        return nil;
    }
}

-(NSMutableArray *) getFile {
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"ConsoleForSearch.plist"];
    NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
    
    return documentDict;
}

-(void)viewWillAppear:(BOOL)animated {
    self.searchGameSB.text = @"";
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.hidesBackButton = YES;
    self.gameListTV.backgroundColor = [UIColor clearColor];
    [self startHUD];
    self.consoles = [self getFile];
    
    [DAO listGameByConsole:@"" andPlatforms:self.consoles andCompletionHandler:^(BOOL success, NSArray *json) {
        self.filteredGames = [json mutableCopy];
        [self.gameListTV reloadData];
        [self.hud hide:YES];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredGames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GameListCell *cell = (GameListCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[GameListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }

    cell.tittleLBL.text = [[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.consoleLBL.text = [[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"platform"];
    cell.valueLBL.text = [[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"price"];
    cell.addressLBL.text = [[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"location"];
    NSData *data = [[NSData alloc]initWithBase64EncodedString:[[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"image"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
    cell.imageView.image =[self imageWithImage:[UIImage imageWithData: data] scaledToSize:CGSizeMake(80.0, 90.0)];
    
    return cell;
}

-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length % 3 == 0 && searchText.length != 0){
        [self startHUD];
        [DAO listGameByConsole:searchText andPlatforms:self.consoles andCompletionHandler:^(BOOL success, NSArray *json) {
            self.filteredGames = [json mutableCopy];
            [self.gameListTV reloadData];
            [self.hud hide:YES];
        }];
    }
    else if(searchText.length == 0) {
        [self startHUD];
        [DAO listGames:^(BOOL success, NSArray *json) {
            self.filteredGames = [json mutableCopy];
            [self.gameListTV reloadData];
            [self.hud hide:YES];
        }];
        [self.searchGameSB resignFirstResponder];
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.segue = @"gameDetail";
    
    GameListCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    self.game.ownerID = [[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"user_id"];
    self.game.gameName = [[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"name"];
    self.game.value = [[self.filteredGames objectAtIndex:indexPath.row] objectForKey:@"price"];
    self.game.gameImage =cell.imageView;
    
    [self performSegueWithIdentifier:self.segue sender:nil];
}

- (IBAction)profile:(UIBarButtonItem *)sender {
    self.segue = @"profile";
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:@"Profile.plist"];
    NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
    
    self.user.userID = [documentDict objectAtIndex:0];
    
    [self performSegueWithIdentifier:self.segue sender:nil];
}

- (IBAction)addGame:(UIBarButtonItem *)sender {
    self.segue = @"addGame";
    [self performSegueWithIdentifier:self.segue sender:nil];
}

- (IBAction)config:(UIBarButtonItem *)sender {
    self.segue = @"config";
    [self performSegueWithIdentifier:self.segue sender:nil];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"gameDetail"]){
        GameDetailVC *gameDetail = [segue destinationViewController];
        gameDetail.game = self.game;
        gameDetail.user = self.user;
        gameDetail.gameImage = self.game.gameImage.image;
    }
    if([segue.identifier isEqualToString:@"profile"]){
        ProfileVC *profile = [segue destinationViewController];
        profile.user = self.user;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
@end
