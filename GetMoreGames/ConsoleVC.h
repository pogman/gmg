//
//  ConsoleVC.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/9/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddGameVC.h"

@protocol senddataProtocol <NSObject>

-(void)sendDataToA:(NSString *)myStringData; //I am thinking my data is NSArray , you can use another object for store your information.

@end

@interface ConsoleVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *consoleTV;

@property (strong, nonatomic) NSMutableArray *consoleArray;

@property (strong, nonatomic)NSString *console;

@property (nonatomic,assign) id delegate;

@end
