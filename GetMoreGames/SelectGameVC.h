//
//  SelectGameVC.h
//  GetMoreGames
//
//  Created by Gustavo Gomes de Oliveira on 5/4/16.
//  Copyright © 2016 Gustavo Gomes de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "GameDeatilUtility.h"
#import "DAO.h"
#import "UserData.h"

@interface SelectGameVC : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *gameListTV;

@property (weak, nonatomic) IBOutlet UISearchBar *searchGameSB;

@property (strong, nonatomic) NSMutableArray *filteredGames;

@property (weak, nonatomic) NSString *segue;

@property (strong, nonatomic) UserData *user;

@property (strong, nonatomic) NSMutableArray *profile;

@property NSMutableArray *consoles;

@property MBProgressHUD *hud;

@property (strong, nonatomic) GameDeatilUtility *game;

- (IBAction)profile:(UIBarButtonItem *)sender;

- (IBAction)addGame:(UIBarButtonItem *)sender;

- (IBAction)config:(UIBarButtonItem *)sender;

@end
